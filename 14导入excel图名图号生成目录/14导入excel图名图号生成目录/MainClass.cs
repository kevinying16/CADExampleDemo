﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using System;
using Autodesk.AutoCAD.ApplicationServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14导入excel图名图号生成目录
{
    public static class MainClass
    {

        public static ObjectId[] CreateDBText(this Database db, String textContents,Point3d InsertPos,double textHeight)
        {
            // 声明单行文本对象
            DBText dbtext = new DBText();


            dbtext.Position = InsertPos;

            dbtext.Height = textHeight;
            dbtext.TextString = textContents;

            return db.AddEntityToModeSpace(dbtext);
        }


        /// <summary>
        /// 添加多个图形对象到图形文件中
        /// </summary>
        /// <param name="db">图形数据库</param>
        /// <param name="ent">图形对象 可变参数 传入一个数组</param>
        /// <returns>图形的ObjectId 数组返回</returns>
        public static ObjectId[] AddEntityToModeSpace(this Database db, params Entity[] ent)
        {
            // 声明ObjectId 用于返回
            ObjectId[] entId = new ObjectId[ent.Length];
            // 开启事务处理
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                // 打开块表
                BlockTable bt = (BlockTable)trans.GetObject(db.BlockTableId, OpenMode.ForRead);
                // 打开块表记录
                BlockTableRecord btr = (BlockTableRecord)trans.GetObject(bt[BlockTableRecord.ModelSpace], OpenMode.ForWrite);

                for (int i = 0; i < ent.Length; i++)
                {
                    // 将图形添加到块表记录
                    entId[i] = btr.AppendEntity(ent[i]);
                    // 更新数据信息
                    trans.AddNewlyCreatedDBObject(ent[i], true);

                }
                // 提交事务
                trans.Commit();
            }
            return entId;
        }


       


        /// <summary>
        /// 获取一个块坐标
        /// </summary>
        /// <param name="db"></param>
        /// <param name="blockName">块名称</param>
        /// <returns></returns>
        public static Point3d GetBlockRefCoor(this Database db,string blockName)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
           
            Editor ed = doc.Editor;
            Point3d blockPos = new Point3d();

            //blockData data = new blockData();

            // 筛选指定名称的块参照
            TypedValue[] values = { new TypedValue((int)DxfCode.Start, "INSERT"),
                                    new TypedValue((int)DxfCode.BlockName, blockName),
                                    };

            var filter = new SelectionFilter(values);
            
            var entSelected = ed.SelectAll(filter);
            
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                // 遍历块参照
                foreach (var id in entSelected.Value.GetObjectIds())
                {
                    BlockReference bref = (BlockReference)trans.GetObject(id, OpenMode.ForRead);
                    //data.X = bref.Position.X;
                    //data.Y = bref.Position.Y;
                    //data.Z = bref.Position.Z;
                    blockPos = bref.Position;
                }
                trans.Commit();
            }
            return blockPos;
        }


        ///// <summary>
        /////  获取同一名称的多个图框块的坐标
        ///// </summary>
        ///// <param name="db"></param>
        ///// <param name="blockName">块名称</param>
        ///// <returns></returns>
        //public static List<Point3d> GetBlockRefCoors(this Database db, string blockName)
        //{
        //    Document doc = Application.DocumentManager.MdiActiveDocument;

        //    Editor ed = doc.Editor;
        //    List<Point3d> blockPosList = new List<Point3d>();

        //    //blockData data = new blockData();

        //    // 筛选指定名称的块参照
        //    TypedValue[] values = { new TypedValue((int)DxfCode.Start, "INSERT"),
        //                            new TypedValue((int)DxfCode.BlockName, blockName),
        //                            };

        //    var filter = new SelectionFilter(values);

        //    var entSelected = ed.SelectAll(filter);

        //    using (Transaction trans = db.TransactionManager.StartTransaction())
        //    {
        //        // 遍历块参照
        //        foreach (var id in entSelected.Value.GetObjectIds())
        //        {
                    
                    
        //            for (int i = 1; i < entSelected.Value.Count; i++)
        //            {
        //                BlockReference bref = (BlockReference)trans.GetObject(id, OpenMode.ForRead);

                        
        //                blockPosList[i] = bref.Position;
        //            }
                   
                    
                    
        //        }
        //        trans.Commit();
        //    }
        //    return blockPosList;
        //}



        /// <summary>
        /// 获取同一块名的多个块坐标
        /// </summary>
        /// <param name="db">数据库对象</param>
        /// <param name="blockName">块名</param>
        /// <returns>返回指定块名的块参照</returns>
        public static List<Point3d> GetAllBlockCoors(this Database db, string blockName)
        {
            List<Point3d> blockcoors = new List<Point3d>();
            Point3d blockcoor = new Point3d();
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                //打开块表
                BlockTable bt = (BlockTable)trans.GetObject(db.BlockTableId, OpenMode.ForRead);
                //打开指定块名的块表记录
                BlockTableRecord btr = (BlockTableRecord)trans.GetObject(bt[blockName], OpenMode.ForRead);
                //获取指定块名的块参照集合的Id
                ObjectIdCollection blockIds = btr.GetBlockReferenceIds(true, true);
                foreach (ObjectId id in blockIds) // 遍历块参照的Id
                {
                    //获取块参照
                    BlockReference block = (BlockReference)trans.GetObject(id, OpenMode.ForRead);
                    blockcoor = block.Position;
                    blockcoors.Add(blockcoor); // 将块参照添加到返回列表 
                }
                trans.Commit();
            }
            return blockcoors; //返回块参照列表
        }
    }
}
